use clap::Parser;
use image::io::Reader as ImageReader;
use lofty::Probe;
use std::{io::Cursor, path::Path};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path to the input file
    #[arg(short, long)]
    input: String,

    /// Path to the input file
    #[arg(short, long)]
    output: String,

    /// Dimension of the cover art to be exported
    #[arg(short, long, default_value_t = 128)]
    size: u32,
}

fn main() {
    let args = Args::parse();

    let in_path = Path::new(&args.input);

    if !in_path.is_file() {
        panic!("ERROR: Path is not a file!");
    }

    let tagged_file = Probe::open(in_path)
        .expect("ERROR: Bad path provided!")
        .read(true)
        .expect("ERROR: Failed to read file!");

    for tag in tagged_file.tags() {
        for pic in tag.pictures() {
            let mut img = ImageReader::new(Cursor::new(pic.data()))
                .with_guessed_format()
                .expect("ERROR: Unable to guess image format of the covert art")
                .decode()
                .expect("ERROR: Unable to decode image data");

            if args.size != 0
            {
                img = img.resize(args.size, args.size, image::imageops::FilterType::Nearest);
            }

            img.save_with_format(&args.output, image::ImageFormat::Png)
                .expect("ERROR: Failed to save album art");
        }
    }
}
